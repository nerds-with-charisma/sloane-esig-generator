import React, { useEffect, useState } from 'react';
import { navigate } from 'gatsby';

import Layout from '../components/layout';
import Tabs from '../components/Tabs';
import Heading from '../components/Heading';
import PersonalForm from '../components/PersonalForm';
import SocialForm from '../components/SocialForm';
import Preview from '../components/Preview';
import ImportForm from '../components/ImportForm';
import StylesForm from '../components/StylesForm';

const IndexPage = ({ location }) => {
  const [base64String, base64StringSetter] = useState(null);
  const [visibleForm, visibleFormSetter] = useState('personal');
  const [jsonImport, jsonImportSetter] = useState({});
  const [styleType, styleTypeSetter] = useState('default');

  const [cardData, cardDataSetter] = useState({
    company: 'Nerds with Charisma',
    name: 'Brian Dausman',
    title: 'JAMstack Jedi and Front-End Developer',
    email: null,
    phone: '(555) 555-5555',
    desk: null,
    fax: null,
    address: null,
    website: 'https://nerdswithcharisma.com',
    logo: 'https://nerdswithcharisma.com/logos/logo--gradient.svg',
  });

  const [socialAccounts, socialAccountsSetter] = useState([
    {
      title: 'Facebook',
      url: 'https://facebook.com/',
      icon: 'Facebook-color',
    },
  ]);

  const [specificStyles, specificStylesSetter] = useState({
    nameSize: 18,
    nameColor: '#2c2c2c',
    titleSize: 15,
    titleColor: '#888',
    phoneSizes: 15,
    phoneColors: '#444',
    socialSize: 18,
    socialColor: '#2c2c2c',
    logoWidth: 80,
    linkColor: '#521dff',
    linkSize: 16,
    borderWidth: 2,
    borderColor: '#eee',
  });

  useEffect(() => {
    if (location.search) {
      const incomingBase64String = location.search.split('?')[1];
      importJson(atob(incomingBase64String));
      visibleFormSetter('import');
    }
  }, []);

  const importJson = jsonData => {
    if (!jsonData) {
      jsonData = JSON.parse(jsonImport);
    } else {
      jsonData = JSON.parse(jsonData);
    }

    base64StringSetter(btoa(JSON.stringify(jsonData))); // convert to base64 for url updating
    navigate(`?${btoa(JSON.stringify(jsonData))}`);

    // set them in state
    cardDataSetter({ ...jsonData.cardData });
    specificStylesSetter({ ...jsonData.specificStyles });
    socialAccountsSetter([...jsonData.socialAccounts]);
    styleTypeSetter(jsonData.styleType);
  };

  return (
    <Layout>
      <Tabs visibleFormSetter={visibleFormSetter} visibleForm={visibleForm} />

      <div id="hero" className="align--center bootstrap-wrapper">
        <div className="container--md">
          <br />
          <div className="row">
            <div className="col-12 col-sm-12 col-md-6 col-lg-6 bg--light radius--sm shadow--xl">
              <br />
              <div className="text--center font--dark">
                <Heading
                  title={`Edit Your ${visibleForm
                    .charAt(0)
                    .toUpperCase()}${visibleForm.slice(1)} Info`}
                />
              </div>
              <br />

              {visibleForm === 'personal' && (
                <PersonalForm
                  cardData={cardData}
                  cardDataSetter={cardDataSetter}
                  visibleFormSetter={visibleFormSetter}
                />
              )}

              {visibleForm === 'social' && (
                <SocialForm
                  socialAccounts={socialAccounts}
                  socialAccountsSetter={socialAccountsSetter}
                  visibleFormSetter={visibleFormSetter}
                />
              )}

              {visibleForm === 'styles' && (
                <StylesForm
                  styleType={styleType}
                  styleTypeSetter={styleTypeSetter}
                  specificStyles={specificStyles}
                  specificStylesSetter={specificStylesSetter}
                  visibleFormSetter={visibleFormSetter}
                />
              )}

              {visibleForm === 'import' && (
                <ImportForm
                  jsonImportSetter={jsonImportSetter}
                  importJson={importJson}
                  jsonImport={jsonImport}
                  location={location}
                  cardData={cardData}
                  socialAccounts={socialAccounts}
                  styleType={styleType}
                  specificStyles={specificStyles}
                  visibleFormSetter={visibleFormSetter}
                />
              )}
              <br />
            </div>

            <div className="col-1" />

            <div className="col-12 col-sm-12 col-md-5 col-lg-5 align--center">
              <Preview
                specificStyles={specificStyles}
                cardData={cardData}
                socialAccounts={socialAccounts}
                styleType={styleType}
              />
            </div>
          </div>
          <br />
        </div>
      </div>
    </Layout>
  );
};

export default IndexPage;
