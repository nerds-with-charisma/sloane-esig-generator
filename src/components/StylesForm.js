import React, { useState } from 'react';
import { SketchPicker } from 'react-color';

import { Input, Select } from '@nerds-with-charisma/nerds-style-sass';

const StylesForm = ({
  styleType,
  styleTypeSetter,
  specificStyles,
  specificStylesSetter,
  visibleFormSetter,
}) => {
  // border
  const [displayBorderColorPicker, displayBorderColorPickerSetter] = useState(
    false
  );
  const [borderColor, borderColorSetter] = useState(specificStyles.borderColor);

  // color picker configs
  const handleBorderClick = () =>
    displayBorderColorPickerSetter(!displayBorderColorPicker);
  const handleBorderClose = () => displayBorderColorPickerSetter(false);
  const handleBorderChange = color => {
    borderColorSetter(color.hex);
    specificStylesSetter({ ...specificStyles, borderColor: color.hex });
  };
  // e: name

  // name
  const [displayNameColorPicker, displayNameColorPickerSetter] = useState(
    false
  );
  const [nameColor, nameColorSetter] = useState(specificStyles.nameColor);

  // color picker configs
  const handleNameClick = () =>
    displayNameColorPickerSetter(!displayNameColorPicker);
  const handleNameClose = () => displayNameColorPickerSetter(false);
  const handleNameChange = color => {
    nameColorSetter(color.hex);
    specificStylesSetter({ ...specificStyles, nameColor: color.hex });
  };
  // e: name

  // title
  const [displayTitleColorPicker, displayTitleColorPickerSetter] = useState(
    false
  );
  const [titleColor, titleColorSetter] = useState(specificStyles.titleColor);

  // color picker configs
  const handleTitleClick = () =>
    displayTitleColorPickerSetter(!displayTitleColorPicker);
  const handleTitleClose = () => displayTitleColorPickerSetter(false);

  const handleTitleChange = color => {
    titleColorSetter(color.hex);
    specificStylesSetter({ ...specificStyles, titleColor: color.hex });
  };
  // e: title

  // phone
  const [displayPhoneColorPicker, displayPhoneColorPickerSetter] = useState(
    false
  );
  const [phoneColor, phoneColorSetter] = useState(specificStyles.phoneColors);

  // color picker configs
  const handlePhoneClick = () =>
    displayPhoneColorPickerSetter(!displayPhoneColorPicker);
  const handlePhoneClose = () => displayPhoneColorPickerSetter(false);

  const handlePhoneChange = color => {
    phoneColorSetter(color.hex);
    specificStylesSetter({ ...specificStyles, phoneColors: color.hex });
  };
  // e: phone

  // social
  const [displaySocialColorPicker, displaySocialColorPickerSetter] = useState(
    false
  );
  const [socialColor, socialColorSetter] = useState(
    specificStyles.socialColors
  );

  // color picker configs
  const handleSocialClick = () =>
    displaySocialColorPickerSetter(!displaySocialColorPicker);
  const handleSocialClose = () => displaySocialColorPickerSetter(false);

  const handleSocialChange = color => {
    socialColorSetter(color.hex);
    specificStylesSetter({ ...specificStyles, socialColor: color.hex });
  };
  // e: social

  // link
  const [displayLinkColorPicker, displayLinkColorPickerSetter] = useState(
    false
  );
  const [linkColor, linkColorSetter] = useState(specificStyles.linkColors);

  // color picker configs
  const handleLinkClick = () =>
    displayLinkColorPickerSetter(!displayLinkColorPicker);
  const handleLinkClose = () => displayLinkColorPickerSetter(false);

  const handleLinkChange = color => {
    linkColorSetter(color.hex);
    specificStylesSetter({ ...specificStyles, linkColor: color.hex });
  };
  // e: link

  return (
    <div>
      <Select
        callback={v => styleTypeSetter(v)}
        fullWidth
        id="select--styleType"
        title="Choose Layout"
        options={[
          {
            title: 'Default Layout',
            value: 'default',
          },
          {
            title: 'Minimal Layout',
            value: 'minimal',
          },
          {
            title: 'Clean Layout',
            value: 'clean',
          },
        ]}
      />

      <br />
      <br />

      <div className="row">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <Input
            title="Border Width"
            id="input--borderWidth"
            callback={v =>
              specificStylesSetter({
                ...specificStyles,
                borderWidth: parseInt(v),
              })
            }
            type="tel"
            populatedValue={specificStyles.borderWidth}
          />
        </div>

        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <button
            type="button"
            className="border--none radius--lg block"
            style={{ background: borderColor, marginTop: 20 }}
            onClick={() => handleBorderClick()}
          >
            <strong>{'Border Color'}</strong>
          </button>

          {displayBorderColorPicker === true ? (
            <div className="position--absolute" style={{ zIndex: 99, left: 0 }}>
              <button
                className="padding--none border--none"
                onClick={() => handleBorderClose()}
              >
                <SketchPicker
                  color={borderColor}
                  onChange={v => handleBorderChange(v)}
                />
              </button>
            </div>
          ) : null}
        </div>
      </div>

      <div className="row">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <Input
            title="Name Font Size"
            id="input--nameFontSize"
            callback={v =>
              specificStylesSetter({ ...specificStyles, nameSize: parseInt(v) })
            }
            type="tel"
            populatedValue={specificStyles.nameSize}
          />
        </div>

        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <button
            type="button"
            className="border--none radius--lg block"
            style={{ background: nameColor, marginTop: 20 }}
            onClick={() => handleNameClick()}
          >
            <strong>{'Name Color'}</strong>
          </button>

          {displayNameColorPicker === true ? (
            <div className="position--absolute" style={{ zIndex: 99, left: 0 }}>
              <button
                className="padding--none border--none"
                onClick={() => handleNameClose()}
              >
                <SketchPicker
                  color={nameColor}
                  onChange={v => handleNameChange(v)}
                />
              </button>
            </div>
          ) : null}
        </div>
      </div>

      <div className="row">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <Input
            title="Title Font Size"
            id="input--titleFontSize"
            callback={v =>
              specificStylesSetter({
                ...specificStyles,
                titleSize: parseInt(v),
              })
            }
            type="tel"
            populatedValue={specificStyles.titleSize}
          />
        </div>

        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <button
            type="button"
            className="border--none radius--lg block"
            style={{ background: titleColor, marginTop: 20 }}
            onClick={() => handleTitleClick()}
          >
            <strong>{'Title Color'}</strong>
          </button>

          {displayTitleColorPicker === true ? (
            <div className="position--absolute" style={{ zIndex: 99, left: 0 }}>
              <button
                className="padding--none border--none"
                onClick={() => handleTitleClose()}
              >
                <SketchPicker
                  color={titleColor}
                  onChange={v => handleTitleChange(v)}
                />
              </button>
            </div>
          ) : null}
        </div>
      </div>

      <div className="row">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <Input
            title="Phone Font Size"
            id="input--phoneFontSize"
            callback={v =>
              specificStylesSetter({
                ...specificStyles,
                phoneSizes: parseInt(v),
              })
            }
            type="tel"
            populatedValue={specificStyles.phoneSizes}
          />
        </div>

        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <button
            type="button"
            className="border--none radius--lg block"
            style={{ background: phoneColor, marginTop: 20 }}
            onClick={() => handlePhoneClick()}
          >
            <strong>{'Phone Colors'}</strong>
          </button>

          {displayPhoneColorPicker === true ? (
            <div className="position--absolute" style={{ zIndex: 99, left: 0 }}>
              <button
                className="padding--none border--none"
                onClick={() => handlePhoneClose()}
              >
                <SketchPicker
                  color={phoneColor}
                  onChange={v => handlePhoneChange(v)}
                />
              </button>
            </div>
          ) : null}
        </div>
      </div>

      <div className="row">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <Input
            title="Link Font Size"
            id="input--socialFontSize"
            callback={v =>
              specificStylesSetter({ ...specificStyles, linkSize: parseInt(v) })
            }
            type="tel"
            populatedValue={specificStyles.socialSize}
          />
        </div>

        <div className="col-12 col-sm-12 col-md-4 col-lg-4">
          <button
            type="button"
            className="border--none radius--lg block"
            style={{ background: specificStyles.socialColor, marginTop: 20 }}
            onClick={() => handleLinkClick()}
          >
            <strong>{'Link Colors'}</strong>
          </button>

          {displayLinkColorPicker === true ? (
            <div className="position--absolute" style={{ zIndex: 99, left: 0 }}>
              <button
                className="padding--none border--none"
                onClick={() => handleLinkClose()}
              >
                <SketchPicker
                  color={linkColor}
                  onChange={v => handleLinkChange(v)}
                />
              </button>
            </div>
          ) : null}
        </div>
      </div>
      <div className="row">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4 clear">
          <Input
            title="Social Font Size"
            id="input--socialFontSize"
            callback={v =>
              specificStylesSetter({
                ...specificStyles,
                socialSize: parseInt(v),
              })
            }
            type="tel"
            populatedValue={specificStyles.socialSize}
          />
        </div>

        <div className="hide col-12 col-sm-12 col-md-4 col-lg-4">
          <button
            type="button"
            className="border--none radius--lg block"
            style={{ background: specificStyles.socialColor, marginTop: 20 }}
            onClick={() => handleSocialClick()}
          >
            <strong>{'Social Colors'}</strong>
          </button>

          {displaySocialColorPicker === true ? (
            <div className="position--absolute" style={{ zIndex: 99, left: 0 }}>
              <button
                className="padding--none border--none"
                onClick={() => handleSocialClose()}
              >
                <SketchPicker
                  color={socialColor}
                  onChange={v => handleSocialChange(v)}
                />
              </button>
            </div>
          ) : null}
        </div>

        <div className="col-12">
          <br />
          <button
            type="button"
            onClick={() => visibleFormSetter('social')}
            className="gradient border--none font--14 radius--sm"
          >
            &laquo;
            {' Social'}
          </button>

          <button
            id="generate"
            className="gradient border--none font--14 radius--sm float--right"
            onClick={() => {
              var wnd = window.open('about:blank', '');
              wnd.document.write(document.getElementById('esig').innerHTML);
              wnd.document.close();
            }}
          >
            <strong>{'Generate eSig'}</strong>
          </button>
        </div>
      </div>
    </div>
  );
};

export default StylesForm;
