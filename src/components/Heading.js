import React from 'react';

const Heading = ({ title }) => (
  <strong>
    {title}
  </strong>
);

export default Heading;
