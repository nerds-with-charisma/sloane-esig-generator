import React, { useState } from 'react';
import { TextArea } from '@nerds-with-charisma/nerds-style-sass';

const ImportForm = ({
  cardData,
  jsonImportSetter,
  location,
  importJson,
  socialAccounts,
  styleType,
  specificStyles,
  visibleFormSetter,
}) => {
  const [template, templateSetter] = useState({
    specificStyles: specificStyles,
    cardData: cardData,
    socialAccounts: socialAccounts,
    styleType: styleType,
  });

  return (
    <div className="row">
      <div className="col-12">
        <TextArea
          title="JSON Import"
          id="input--jsonImport"
          callback={v => jsonImportSetter(v)}
          error={null}
          rows={20}
          populatedValue={JSON.stringify(template, undefined, 4)}
          templateSetter={templateSetter}
        />
        <br />
        <div className="text--right">
          <button
            type="button"
            className="gradient border--none font--14 radius--sm"
            onClick={() => {
              importJson(document.getElementById('input--jsonImport').value);
              visibleFormSetter('personal');
            }}
          >
            <strong>{location.search ? 'Import Saved Data' : 'Import'}</strong>
          </button>
        </div>
      </div>
    </div>
  );
};

export default ImportForm;
