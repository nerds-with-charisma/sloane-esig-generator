import React from 'react';

const Default = ({ cardData, socialAccounts, specificStyles }) => (
  <table style={{ ...currentStyles.tableStyles, color: '#000' }}>
    <tbody>
      <tr>
        <td
          width={specificStyles.logoWidth + 20}
          style={{
            ...currentStyles.minimalTitleStyle,
            ...currentStyles.reset,
            textAlign: 'center',
          }}
        >
          {cardData.logo && (
            <a href={cardData.website} style={currentStyles.linkStyles}>
              <img
                src={cardData.logo}
                alt={cardData.company}
                width={`${specificStyles.logoWidth}px`}
              />
            </a>
          )}
        </td>

        <td
          colspan="5"
          style={{
            ...currentStyles.reset,
            borderLeft: `${specificStyles.borderWidth}px solid ${specificStyles.borderColor}`,
            paddingLeft: 15,
          }}
        >
          {cardData.name && (
            <strong
              style={{
                fontSize: specificStyles.nameSize,
                color: specificStyles.nameColor,
              }}
            >
              {cardData.name}
              <br />
            </strong>
          )}

          {cardData.title && (
            <span
              style={{
                fontSize: specificStyles.titleSize,
                color: specificStyles.titleColor,
              }}
            >
              {cardData.title}
              <br />
            </span>
          )}

          <div
            style={{
              lineHeight: `${specificStyles.phoneSizes + 5}px`,
              fontSize: specificStyles.phoneSizes,
              color: specificStyles.phoneColors,
            }}
          >
            {cardData.phone && (
              <React.Fragment>{`${cardData.phone}`}</React.Fragment>
            )}
            <span style={{ color: specificStyles.borderColor }}>
              &nbsp;&nbsp;|&nbsp;&nbsp;
            </span>
            {cardData.company && (
              <a
                href={cardData.website}
                style={{
                  color: specificStyles.linkColor,
                  fontSize: specificStyles.linkSize,
                }}
              >
                {cardData.company}
                <br />
              </a>
            )}
            {cardData.desk && (
              <React.Fragment>
                <strong>{'Desk: '}</strong>
                {cardData.desk}
                <br />
              </React.Fragment>
            )}
            {cardData.fax && (
              <React.Fragment>
                <strong>{'Fax: '}</strong>
                {cardData.fax}
                <br />
              </React.Fragment>
            )}
            {cardData.address && (
              <React.Fragment>
                {cardData.address}
                <br />
              </React.Fragment>
            )}
            {cardData.email && (
              <React.Fragment>
                <a
                  href={`mailto:${cardData.email}`}
                  style={{ color: specificStyles.linkColor }}
                >
                  {cardData.email}
                </a>
                <br />
              </React.Fragment>
            )}
            <br />
            {socialAccounts &&
              socialAccounts.length > 0 &&
              socialAccounts[0].url !== '' &&
              socialAccounts.map(item => (
                <span style={currentStyles.socialStyles}>
                  <a href={item.url} style={currentStyles.linkStyles}>
                    <img
                      src={`/${item.icon}.png`}
                      alt={item.url}
                      width={specificStyles.socialSize}
                    />
                  </a>
                </span>
              ))}
          </div>
        </td>
      </tr>
    </tbody>
  </table>
);

const currentStyles = {
  reset: {
    border: 0,
    padding: 0,
    verticalAlign: 'middle',
  },
  tableStyles: {
    textAlign: 'left',
    color: '#484A47',
    fontSize: '12px',
    lineHeight: '21px',
    padding: '10px 40px',
  },
  imageStyles: {
    magin: 'auto',
    paddingRight: 20,
  },
  titleStyle: { paddingTop: 40 },
  minimalTitleStyle: { fontSize: 16 },
  linkStyles: {
    color: '#8FC0A9',
    textDecoration: 'none',
  },
  socialStyles: {
    paddingRight: 12,
    width: 28,
  },
};

export default Default;
