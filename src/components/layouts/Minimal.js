import React from 'react';

const Minimal = ({ cardData, socialAccounts, specificStyles }) => (
  <table style={{ ...currentStyles.tableStyles, color: '#000' }}>
    <tbody>
      <tr>
        <td
          style={{ ...currentStyles.minimalTitleStyle, ...currentStyles.reset }}
        >
          {cardData.name && (
            <strong
              style={{
                fontSize: specificStyles.nameSize,
                color: specificStyles.nameColor,
              }}
            >
              {cardData.name}
              <br />
            </strong>
          )}
          {cardData.title && (
            <span
              style={{
                fontSize: specificStyles.titleSize,
                color: specificStyles.titleColor,
              }}
            >
              {cardData.title}
            </span>
          )}
          <div
            style={{
              lineHeight: `${specificStyles.phoneSizes + 5}px`,
              fontSize: specificStyles.phoneSizes,
              color: specificStyles.phoneColors,
            }}
          >
            {cardData.phone && (
              <React.Fragment>{`${cardData.phone}`}</React.Fragment>
            )}
            {cardData.desk && (
              <React.Fragment>
                <br />
                <strong>{'Desk: '}</strong>
                {cardData.desk}
              </React.Fragment>
            )}
            {cardData.fax && (
              <React.Fragment>
                <br />
                <strong>{'Fax: '}</strong>
                {cardData.fax}
                <br />
              </React.Fragment>
            )}
            {cardData.address && (
              <React.Fragment>
                <br />
                {cardData.address}
                <br />
              </React.Fragment>
            )}
            {cardData.email && (
              <React.Fragment>
                <br />
                <a
                  href={`mailto:${cardData.email}`}
                  style={{ color: specificStyles.linkColor }}
                >
                  {cardData.email}
                </a>
                <br />
              </React.Fragment>
            )}
          </div>
        </td>
      </tr>
      <tr>
        <td style={{ ...currentStyles.reset }}>
          <table style={{ ...currentStyles.tableStyles }}>
            <tr>
              <td
                width={specificStyles.logoWidth + 20}
                style={{
                  ...currentStyles.minimalTitleStyle,
                  ...currentStyles.reset,
                }}
              >
                <br />
                <a href={cardData.website} style={currentStyles.linkStyles}>
                  <img
                    src={cardData.logo}
                    alt={cardData.company}
                    width={`${specificStyles.logoWidth}px`}
                  />
                </a>

                <br />
                <br />
                {socialAccounts &&
                  socialAccounts.length > 0 &&
                  socialAccounts[0].url !== '' &&
                  socialAccounts.map(item => (
                    <span style={currentStyles.socialStyles}>
                      <a href={item.url} style={currentStyles.linkStyles}>
                        <img
                          src={`/${item.icon}.png`}
                          alt={item.url}
                          width={specificStyles.socialSize}
                        />
                      </a>
                    </span>
                  ))}
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
);

const currentStyles = {
  reset: { border: 0, padding: 0, verticalAlign: 'middle' },
  tableStyles: {
    textAlign: 'left',
    color: '#484A47',
    fontSize: '12px',
    lineHeight: '21px',
    padding: '10px 40px',
  },
  imageStyles: {
    magin: 'auto',
    paddingRight: 20,
  },
  titleStyle: { paddingTop: 40, fontSize: 22 },
  minimalTitleStyle: { fontSize: 16 },
  linkStyles: {
    color: '#8FC0A9',
    textDecoration: 'none',
  },
  socialStyles: {
    paddingRight: 12,
    width: 28,
  },
};

export default Minimal;
