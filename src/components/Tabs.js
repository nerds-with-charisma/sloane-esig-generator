import React from 'react';
import { PropTypes } from 'prop-types';

import Logo from '../images/logo.svg';

const Tabs = ({ visibleFormSetter, visibleForm }) => (
  <nav id="tabBar" className="shadow--md">
    <div className="container--md grid">
      <div className="col-6 col-sm-6 col-md-3 col-lg-3">
        <img src={Logo} alt="Logo" width="150" />
      </div>
      <div className="col-6 hidden--xs hidden--sm">
        <button
          className={
            visibleForm !== 'personal'
              ? 'font--dark border--none font--16 radius--lg'
              : 'active font--primary border--none font--16 radius--lg'
          }
          onClick={() => visibleFormSetter('personal')}
        >
          <strong>{'Personal'}</strong>
        </button>
        <button
          className={
            visibleForm !== 'social'
              ? 'font--dark border--none font--16 radius--lg'
              : 'active font--primary border--none font--16 radius--lg'
          }
          onClick={() => visibleFormSetter('social')}
        >
          <strong>{'Social'}</strong>
        </button>
        <button
          className={
            visibleForm !== 'styles'
              ? 'font--dark border--none font--16 radius--lg'
              : 'active font--primary border--none font--16 radius--lg'
          }
          onClick={() => visibleFormSetter('styles')}
        >
          <strong>{'Styles'}</strong>
        </button>
        <button
          className={
            visibleForm !== 'import'
              ? 'font--dark border--none font--16 radius--lg'
              : 'active font--primary border--none font--16 radius--lg'
          }
          onClick={() => visibleFormSetter('import')}
        >
          <strong>{'Import / Export'}</strong>
        </button>
      </div>

      <div className="col-6 col-sm-6 col-md-3 col-lg-3 text--right">
        <button
          id="generate"
          className="border--none font--14 radius--sm lh--md"
          onClick={() => {
            var wnd = window.open('about:blank', '');
            wnd.document.write(document.getElementById('esig').innerHTML);
            wnd.document.close();
          }}
        >
          <strong>{'Generate eSig'}</strong>
        </button>
      </div>
    </div>
  </nav>
);

Tabs.propTypes = {
  visibleFormSetter: PropTypes.func.isRequired,
};

export default Tabs;
