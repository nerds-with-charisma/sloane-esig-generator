import React from 'react';

import { Input, maskPhoneNumber } from '@nerds-with-charisma/nerds-style-sass';

const PersonalForm = ({ cardData, cardDataSetter, visibleFormSetter }) => (
  <div className="row">
    <Input
      title="Logo (80px)"
      id="input--logo"
      callback={v => cardDataSetter({ ...cardData, logo: v })}
      error={null}
      type="text"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="https://d33wubrfki0l68.cloudfront.net/7380a5b5b8a8d475e1dc4fe2d7c2e869ae89991c/943bf/404/logo--nwc-purple.svg"
      populatedValue={cardData.logo}
    />

    <Input
      title="Company"
      id="input--company"
      callback={v => cardDataSetter({ ...cardData, company: v })}
      error={null}
      type="text"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="Nerds with Charisma"
      populatedValue={cardData.company}
    />

    <Input
      title="Name"
      id="input--name"
      callback={v => cardDataSetter({ ...cardData, name: v })}
      error={null}
      type="text"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="Brian Dausman"
      populatedValue={cardData.name}
    />

    <Input
      title="Title"
      id="input--title"
      callback={v => cardDataSetter({ ...cardData, title: v })}
      error={null}
      type="text"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="Jamstack Jedi and Front-End Developer"
      populatedValue={cardData.title}
    />

    <Input
      title="Phone"
      id="input--phone"
      callback={v => {
        cardDataSetter({ ...cardData, phone: maskPhoneNumber(v) });
        document.getElementById('input--phone').value = maskPhoneNumber(v);
      }}
      error={null}
      type="tel"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="(555) 555-5555"
      populatedValue={cardData.phone}
    />

    <Input
      title="Website"
      id="input--website"
      callback={v => cardDataSetter({ ...cardData, website: v })}
      error={null}
      type="text"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="https://nerdswithcharisma.com"
      populatedValue={cardData.website}
    />

    <Input
      title="Email (prob don't need this)"
      id="input--email"
      callback={v => cardDataSetter({ ...cardData, email: v })}
      error={null}
      type="email"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="briandausman@gmail.com"
      populatedValue={cardData.email}
    />

    <Input
      title="Desk Phone"
      id="input--desk"
      callback={v => {
        cardDataSetter({ ...cardData, desk: maskPhoneNumber(v) });
        document.getElementById('input--desk').value = maskPhoneNumber(v);
      }}
      error={null}
      type="tel"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="(555) 555-5555"
      populatedValue={cardData.desk}
    />

    <Input
      title="Fax"
      id="input--fax"
      callback={v => {
        cardDataSetter({ ...cardData, fax: maskPhoneNumber(v) });
        document.getElementById('input--fax').value = maskPhoneNumber(v);
      }}
      error={null}
      type="tel"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder="(555) 555-5555"
      populatedValue={cardData.fax}
    />

    <Input
      title="Address"
      id="input--address"
      callback={v => cardDataSetter({ ...cardData, address: v })}
      error={null}
      type="text"
      theme="col-12 col-sm-12 col-md-6 col-lg-6"
      placeHolder=""
      populatedValue={cardData.address}
    />

    <div className="col-12 text--right">
      <br />
      <button
        type="button"
        onClick={() => visibleFormSetter('social')}
        className="gradient border--none font--14 radius--sm"
      >
        {'Go to Social '}
        &raquo;
      </button>
    </div>
  </div>
);

export default PersonalForm;
