import React, { useState } from 'react';

import GmailLogo from '../images/icon--gmail.png';
import OutlookLogo from '../images/icon--outlook.png';
import OutlookWebLogo from '../images/icon--365.png';
import AppleMailLogo from '../images/icon--apple.png';

import FillIt from '../images/about--fillIt.jpg';
import Social from '../images/about--social.jpg';
import Styles from '../images/about--styles.jpg';
import Generate from '../images/about--generate.jpg';
import Import from '../images/about--import.jpg';

const About = () => {
  const [testIt, testItSetter] = useState('gmail');

  return (
    <div className="container--md bootstrap-wrapper">
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className="row">
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center align--center">
          <div>
            <h2 className="font--dark font--50">{'Fill It Out'}</h2>
            <br />
            <p className="font--21 font--text">
              Enter your personal data on the first tab. Fill out as much or as
              little as you like and we'll auto-hide what you don't need. Since
              we don't host anything, and your logo will be in your email for
              the long haul, we need an absolute url to it. So upload it
              somewhere on your server or company web site and use the link in
              the logo field.
            </p>
          </div>
        </div>
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center">
          <img src={FillIt} alt="Fill it out" className="responsive" />
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className="row">
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center">
          <img src={Social} alt="Fill social out" className="responsive" />
        </div>
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center align--center">
          <div>
            <h2 className="font--dark font--50">{'Be Social'}</h2>
            <br />
            <p className="font--21 font--text">
              Add all your social media accounts. Add as many or as little as
              you'd like (you can add more with the '+ Add' button). You can use
              any social network supported by{' '}
              <a
                href="https://fontawesome.com/icons"
                target="_blank"
                rel="noopener noreferrer"
              >
                font-awesome
              </a>
              . Once satisfied, continue to the styles section to customize the
              look &amp; feel of your signature.
            </p>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className="row">
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center align--center">
          <div>
            <h2 className="font--dark font--50">{'Style It Up'}</h2>
            <br />
            <p className="font--21 font--text">
              Now's the time to make it look good and follow your brand
              guidelines. There are several layout templates you can choose in
              with the dropdown at the top. Then you can customize the font
              sizes and colors to really make it yours.
            </p>
          </div>
        </div>
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center">
          <img src={Styles} alt="Fill styles out" className="responsive" />
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className="row">
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center">
          <img src={Generate} alt="Generate It" className="responsive" />
        </div>
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center align--center">
          <div>
            <h2 className="font--dark font--50">{'Generate It'}</h2>
            <br />
            <p className="font--21 font--text">
              Once you're satisfied with the content and styles, click the{' '}
              <strong>Generate</strong> button, we'll bundle everything
              together, open a new window, and present you with your fancy new
              email siganture. From here you can save it as HTML or select all >
              copy > paste it into your favorite email client. If you choose to
              download the HTML you can save it for later and make adjustments
              to the layout itself.
            </p>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <div className="row">
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center">
          <h2 className="font--dark font--50">{'USE IT!'}</h2>
          <br />
          <p className="font--21 font--text">
            Once generated, you can add it to your favorite email client quickly
            and easily. Outlook, outlook web, gmail, apple mail:
            <br />
            <br />
            <button
              type="button"
              onClick={() => testItSetter('gmail')}
              className={
                testIt === 'gmail'
                  ? 'padding--none radius--lg border--none'
                  : ' opacity--3 padding--none radius--lg border--none'
              }
            >
              <img src={GmailLogo} width="75" alt="" />
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button
              type="button"
              onClick={() => testItSetter('outlook')}
              className={
                testIt === 'outlook'
                  ? 'padding--none radius--lg border--none'
                  : ' opacity--3 padding--none radius--lg border--none'
              }
            >
              <img src={OutlookLogo} width="75" alt="" />
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button
              type="button"
              onClick={() => testItSetter('365')}
              className={
                testIt === '365'
                  ? 'padding--none radius--lg border--none'
                  : ' opacity--3 padding--none radius--lg border--none'
              }
            >
              <img src={OutlookWebLogo} width="75" alt="" />
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button
              type="button"
              onClick={() => testItSetter('apple')}
              className={
                testIt === 'apple'
                  ? 'padding--none radius--lg border--none'
                  : ' opacity--3 padding--none radius--lg border--none'
              }
            >
              <img src={AppleMailLogo} width="75" alt="" />
            </button>
          </p>
        </div>
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center shadow--sm radius--md padding--lg">
          {testIt === 'gmail' && (
            <section>
              <h2>{'Gmail'}</h2>
              <ul className="text--left font--14 lh--lg">
                <li>
                  • Generate your signature and copy the whole thing by using{' '}
                  <u>cmd+a</u>. Copy the signature with <u>cmd+c</u>.
                </li>
                <li>
                  • Head over to <u>Gmail</u> and click the <u>settings cog</u>{' '}
                  at the upper right, choose <u>Settings</u>.
                </li>
                <li>
                  • Under <u>general</u>, scroll down until you find{' '}
                  <u>Signautre</u>.{' '}
                </li>
                <li>
                  • Click the <u>radio button</u> to select the input and paste
                  the signature in with <u>cmd+v</u>.
                </li>
                <li>
                  • Confirm that the <u>Always display external images</u> radio
                  is selected.
                </li>
                <li>
                  • Scroll to the bottom and click <u>Save</u>. That's it!
                  You're good to go!
                </li>
              </ul>
            </section>
          )}

          {testIt === 'outlook' && (
            <section>
              <h2>{'Outlook App'}</h2>
              <p className="text--left font--14">
                Outlook makes it a little harder to add a new signature, here's
                a video by <u>Email Signature Rescue</u> walking you through the
                process.
              </p>
              <br />
              <iframe
                title="outlook email signature"
                width="560"
                height="315"
                src="https://www.youtube.com/embed/uu0hBvBSoJ8"
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              />
            </section>
          )}

          {testIt === '365' && (
            <section>
              <h2>{'Outlook 365 Web'}</h2>
              <ul className="text--left font--14 lh--lg">
                <li>
                  • Generate your signature and copy the whole thing by using{' '}
                  <u>cmd+a</u>. Copy the signature with <u>cmd+c</u>.
                </li>
                <li>
                  • Open up your Outlook web-mail and select the{' '}
                  <u>settings cog</u> at the top right.
                </li>
                <li>
                  • Search for <u>Email Signature</u>, and click it in the
                  search results.
                </li>
                <li>
                  • Paste your signature into the input with <u>cmd+v</u>, then
                  check the 2 checkboxes to include it in new and outgoing
                  emails.
                </li>
                <li>
                  • Click <u>Save</u> at the bottom. That's it! You're good to
                  go!
                </li>
              </ul>
            </section>
          )}

          {testIt === 'apple' && (
            <section>
              <h2>{'Apple Mail'}</h2>
              <ul className="text--left font--14 lh--lg">
                <li>
                  • Generate your signature, right click and choose{' '}
                  <u>View source</u> and copy the whole thing by using{' '}
                  <u>cmd+a</u>. Copy the signature with <u>cmd+c</u>.
                </li>
                <li>
                  • Open up Apple mail and navigate to{' '}
                  <u>Mail > Preferences > Signatures > +</u> to create a new
                  signature.
                </li>
                <li>
                  • Name it something unique and close this box and <u>quit</u>
                  Apple Mail.
                </li>
                <li>
                  • Open up <u>Finder</u> and at the top nav select{' '}
                  <u>Go > Go To Folder</u> and type in <u>~/Library/Mail/</u>
                </li>
                <li>
                  • Open the highest numbered <u>V</u> folder then go to{' '}
                  <u>MailData > Signatures</u> and open the latest created file
                  in a text editor.
                </li>
                <li>
                  • Delete the entire <u>body</u> section. Paste your email
                  signature code into this file and save it.
                </li>
                <li>
                  • That's it! You're good to go! If you get an error to unlock
                  the file, it's in the title bar next to the filename at the
                  top.
                </li>
              </ul>
            </section>
          )}
        </div>
      </div>

      <br />
      <br />
      <br />
      <br />
      <br />
      <br />

      <div className="row">
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center">
          <img src={Import} alt="Import / Export" className="responsive" />
        </div>
        <div className="col-12 col-sm-12 col-md-6 col-lg-6 text--center align--center">
          <div>
            <h2 className="font--dark font--50">{'Import It / Export It'}</h2>
            <br />
            <p className="font--21 font--text">
              Maybe a filling out forms isn't for you. That's cool, simply go to
              the import/export tab, edit the json directly and it will update
              live. This can be handy if you have to create signatures for your
              whole team, you can quickly update everyone's info, using the same
              style properties, and import them individually when you're ready
              to create them all.
            </p>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
};

export default About;
