import React from 'react';

import { Input } from '@nerds-with-charisma/nerds-style-sass';

const SocialForm = ({
  socialAccounts,
  socialAccountsSetter,
  visibleFormSetter,
}) => (
  <div>
    <div className="font--14 col-12">
      We use an icon set from dribbble user{' '}
      <a
        href="https://dribbble.com/shots/2235197-50-Free-Flat-Icons-Sketch-Illustrator"
        target="_blank"
      >
        Alexis Doreau 🤘
      </a>
      <br />
      Simply type the name followed by a dash, then 'color' or 'black'.
    </div>
    <br />
    {socialAccounts.map((field, i) => (
      <div key={i} className="row">
        {/*
        <Input
          title={`Social Network ${i + 1}`}
          id={`input--title-${i}`}
          callback={(v) => {
            let socialAccountsMute = socialAccounts;
            socialAccountsMute[i].title = v;
            socialAccountsSetter(socialAccounts => [...socialAccounts], socialAccountsMute)}
          }
          error={null}
          type="text"
          populatedValue={field.title}
          placeHolder="Facebook"
          theme="col-4"
        />
        */}

        <Input
          title="Url"
          id={`input--url-${i}`}
          callback={v => {
            let socialAccountsMute = socialAccounts;
            socialAccountsMute[i].url = v;
            socialAccountsSetter(
              socialAccounts => [...socialAccounts],
              socialAccountsMute
            );
          }}
          error={null}
          type="text"
          populatedValue={field.url}
          placeHolder="http://facebook.com"
          theme="col-12 col-sm-12 col-md-6 col-lg-6"
        />

        <Input
          title="Icon"
          id={`input--icon-${i}`}
          callback={v => {
            let socialAccountsMute = socialAccounts;
            socialAccountsMute[i].icon = v;
            socialAccountsSetter(
              socialAccounts => [...socialAccounts],
              socialAccountsMute
            );
          }}
          error={null}
          type="text"
          populatedValue={field.icon}
          placeHolder="fab fa-facebook-square"
          theme="col-12 col-sm-12 col-md-6 col-lg-6"
        />
      </div>
    ))}

    <div className="row">
      <div className="col-12 text--right">
        <button
          type="button"
          className="font--dark border--none radius--lg font--14"
          onClick={() => {
            let socialAccountsMute = socialAccounts;
            socialAccountsMute.push({});
            socialAccountsSetter(
              socialAccounts => [...socialAccounts],
              socialAccountsMute
            );
          }}
        >
          {'+ Add'}
        </button>
      </div>

      <div className="col-12 text--right">
        <br />
        <button
          type="button"
          onClick={() => visibleFormSetter('personal')}
          className="gradient border--none font--14 radius--sm float--left"
        >
          &laquo;
          {' Personal'}
        </button>

        <button
          type="button"
          onClick={() => visibleFormSetter('styles')}
          className="gradient border--none font--14 radius--sm"
        >
          {'Go to Styles '}
          &raquo;
        </button>
      </div>
    </div>
  </div>
);

export default SocialForm;
