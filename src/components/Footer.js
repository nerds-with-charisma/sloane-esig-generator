import React from 'react';

import Logo from '../images/logo.svg';

const Footer = () => (
  <footer className="container--md">
    <div className="row">
      <div className="col-xs-12 font--14 font--text text--center">
        <img src={Logo} alt="Logo" width="100" />

        <br />
        <br />
        <p>
          {'Copyright '}
          &copy;&nbsp;
          {new Date().getFullYear()}
          <a href="https://nerdswithcharisma.com" className="font--primary">
            {' Nerds with Charisma'}
          </a>
          <br />
          <br />
          {'Developed by Brian Dausman'}
        </p>
        <br />
        <br />
        <br />
      </div>
    </div>
  </footer>
);

export default Footer;
