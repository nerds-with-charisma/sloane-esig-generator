import React from 'react';

import Default from './layouts/Default';
import Minimal from './layouts/Minimal';
import Clean from './layouts/Clean';

const Preview = ({ cardData, socialAccounts, styleType, specificStyles }) => (
  <div id="esig">
    <div
      className="bg--light radius--sm shadow--xl padding--md"
      style={{
        fontFamily:
          '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
      }}
    >
      {styleType === 'default' && (
        <Default
          specificStyles={specificStyles}
          cardData={cardData}
          socialAccounts={socialAccounts}
          styleType={styleType}
        />
      )}

      {styleType === 'minimal' && (
        <Minimal
          specificStyles={specificStyles}
          cardData={cardData}
          socialAccounts={socialAccounts}
          styleType={styleType}
        />
      )}

      {styleType === 'clean' && (
        <Clean
          specificStyles={specificStyles}
          cardData={cardData}
          socialAccounts={socialAccounts}
          styleType={styleType}
        />
      )}
    </div>
  </div>
);

export default Preview;
